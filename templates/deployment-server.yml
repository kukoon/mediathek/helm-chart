---
{{ $nameServerSecret := .Values.server.config.webserver.session.existingSecret.secretName | default (print (include "mediathek.fullname" .) "-server-jwt-secret") }}
{{ $nameOMESecret := .Values.server.config.oven.client.existingSecret.secretName }}
{{- if empty $nameOMESecret }}
{{- if .Values.ovenmediaengine.enabled }}
{{-  $nameOMESecret = print (include "mediathek.fullname" .Subcharts.ovenmediaengine ) "-api-token" }}
{{- else }}
{{-  $nameOMESecret = "ovenmediaengine-token" }}
{{- end }}
{{- end }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "mediathek.fullname" . }}-server
  labels:
    type: "server"
    {{- with .Values.server.labels }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  replicas: {{ .Values.server.replicaCount }}
  selector:
    matchLabels:
      type: "server"
      {{- with .Values.server.labels }}
      {{- toYaml . | nindent 6 }}
      {{- end }}
  template:
    metadata:
      labels:
        type: "server"
        {{- with .Values.server.labels }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
      annotations:
        {{- with .Values.server.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        confighash: {{ .Values.server.config | toYaml | sha256sum | trunc 32 }}
    spec:
      {{- with .Values.server.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}-server
          securityContext:
            {{- toYaml .Values.server.securityContext | nindent 12 }}
          image: "{{ .Values.server.image.repository }}:{{ .Values.server.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.server.image.pullPolicy }}
          args:
            - "-c"
            - "/config.yaml"
          ports:
            - name: http
              containerPort: {{ .Values.server.config.webserver.listen }}
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /api/status
              port: http
          readinessProbe:
            httpGet:
              path: /api/status
              port: http
          envFrom:
            - configMapRef:
                name: {{ include "mediathek.fullname" . }}-server-config-env
          env:
            - name: "MEDIA_SERVER_OVEN__CLIENT__TOKEN"
              valueFrom:
                secretKeyRef:
                  name: "{{ $nameOMESecret }}"
                  key: "{{ .Values.server.config.oven.client.existingSecret.tokenKey }}"
            - name: "MEDIA_SERVER_WEBSERVER__SESSION__SECRET"
              valueFrom:
                secretKeyRef:
                  name: "{{ $nameServerSecret }}"
                  key: "{{ .Values.server.config.webserver.session.existingSecret.secretKey }}"
            - name: "MEDIA_SERVER_DATABASE__CONNECTION__USERNAME"
              valueFrom:
                secretKeyRef:
                  name: "{{ .Values.server.config.database.connection.existingSecret.secretName }}"
                  key: "{{ .Values.server.config.database.connection.existingSecret.usernameKey }}"
            - name: "MEDIA_SERVER_DATABASE__CONNECTION__PASSWORD"
              valueFrom:
                secretKeyRef:
                  name: "{{ .Values.server.config.database.connection.existingSecret.secretName }}"
                  key: "{{ .Values.server.config.database.connection.existingSecret.passwordKey }}"

          resources:
            {{- toYaml .Values.server.resources | nindent 12 }}
          volumeMounts:
            - name: "server-config"
              mountPath: "/config.yaml"
              subPath: "config.yaml"
              readOnly: true
            {{- if .Values.server.database.cockroachdb.enabled }}
            - name: "crdb-certs"
              mountPath: "/certs"
              readOnly: true
            {{- end }}
      volumes:
        - name: "server-config"
          configMap:
            name: {{ include "mediathek.fullname" . }}-server-configfile
            optional: false
        {{- if .Values.server.database.cockroachdb.enabled }}
        - name: "crdb-certs"
          secret:
            secretName: "{{ include "mediathek.fullname" . }}-cockroachdb-root"
            optional: false
        {{- end }}
      {{- with .Values.server.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.server.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.server.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
